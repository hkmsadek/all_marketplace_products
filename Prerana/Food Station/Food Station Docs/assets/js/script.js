// Mobile Menu iteam select
$(document).ready(function () {
    $("._left_menu_name").click(function () {
        $(this).toggleClass("_left_menu_text_open").siblings().slideToggle(300);
    })
});

//SCROLL MENU ONE
$(document).ready(function(){
  var scrollTop = 0;
  $(window).scroll(function(){
    scrollTop = $(window).scrollTop();
    if (scrollTop >= 80) {
      $('#_1menu').addClass( "_menu_bg" );
    } else  {
      $('#_1menu').removeClass("_menu_bg");
    } 
  });
});

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}

$("._left_menu_text_link").first().addClass("_left_menu_text_active");

$("._left_menu_text_link").click(function() {
  $("._left_menu_text_link").removeClass("_left_menu_text_active");
  $(this).addClass("_left_menu_text_active");
});


